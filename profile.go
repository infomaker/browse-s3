package browses3

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"gopkg.in/ini.v1"
)

func AssumeProfile(sess *session.Session, profile string) (*session.Session, error) {
	if profile == "" {
		return sess, nil
	}

	file, err := ini.Load(filepath.Join(os.Getenv("HOME"), ".aws/config"))
	if err != nil {
		return nil, fmt.Errorf("failed to load AWS SDK config: %w", err)
	}

	conf := sess.Config.Copy()

	sec := file.Section("profile " + profile)

	roleARN := sec.Key("role_arn").String()
	region := sec.Key("region").String()
	sourceProfile := sec.Key("source_profile").String()

	if region != "" {
		conf.Region = &region
	}

	// Let the AWS_REGION override profile region
	envRegion := os.Getenv("AWS_REGION")
	if envRegion != "" {
		conf.Region = &envRegion
	}

	if sourceProfile != "" {
		conf.Credentials = credentials.NewSharedCredentials("", sourceProfile)
	}

	if roleARN != "" {
		stsSess, err := session.NewSession(conf.Copy())
		if err != nil {
			return nil, fmt.Errorf(
				"failed to create session for assuming role: %w", err)
		}

		creds := stscreds.NewCredentials(stsSess, roleARN)
		conf.WithCredentials(creds)
	}

	profileSess, err := session.NewSession(conf)
	if err != nil {
		return nil, fmt.Errorf(
			"failed to create session with assumed role: %w", err)
	}

	return profileSess, nil
}
