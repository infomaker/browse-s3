package browses3

import (
	"fmt"

	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
)

type errMsg error

type inputModel struct {
	textInput  textinput.Model
	err        error
	parent     tea.Model
	updateFunc func(value string)
	label      string
}

func initialModel(label string, value string) inputModel {
	ti := textinput.NewModel()
	ti.Placeholder = value
	ti.SetValue(value)
	ti.Focus()
	ti.CharLimit = 200
	ti.Width = 200

	return inputModel{
		label:     label,
		textInput: ti,
		err:       nil,
	}
}

func (m inputModel) Init() tea.Cmd {
	return textinput.Blink
}

func (m inputModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.Type {
		case tea.KeyEnter, tea.KeyCtrlC, tea.KeyEsc:
			m.updateFunc(m.textInput.Value())
			return m.parent, nil
		}

	// We handle errors just like any other Message
	case errMsg:
		m.err = msg
		return m, nil
	}

	m.textInput, cmd = m.textInput.Update(msg)
	return m, cmd
}

func (m inputModel) View() string {
	return fmt.Sprintf(
		"%s\n\n%s\n\n%s",
		m.label, m.textInput.View(),
		"(esc to quit)",
	) + "\n"
}
