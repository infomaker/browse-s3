# browse-s3 CLI

`browse-s3` tool is used for accessing S3. The browse-s3 CLI tool can do the following: 

- browse buckets 
- show content of files
- open image file
- copy file to local storage
- use prefix/filter
- date 

# Example
    ❯ ./browse-s3 --help
    NAME:
    browse-s3 - A new cli application
    
    USAGE:
    browse-s3 [global options] command [command options] [arguments...]
    
    DESCRIPTION:
    Browse AWS S3
    
    COMMANDS:
    help, h  Shows a list of commands or help for one command
    
    GLOBAL OPTIONS:
    --profile value  aws profile to use for access
    --bucket value   bucket to browse optional with path
    --filter value   regexp for bucket/file names
    --day value      date string (YYYY-MM-DD) to match buckets/files only if bucket is set
    --help, -h       show help (default: false)


## Browse files in a bucket 
    
    browse-s3 --profile im-saasstage  --bucket im-saas-env-configurationbucket-179gyf6ri972i

## List buckets
    
    browse-s3 --profile im-saasstage --filter ".*wire.*"


## Files in a specific bucket folder

    browse-s3 --profile im-saastage --bucket wireingest-filearea-1x1v8ekfnrc4z/ktd/wam/

    browse-s3 --profile im-saastage --bucket wireingest-filearea-1x1v8ekfnrc4z/ktd/wam/ --filter ".*xml"


## Demo CCA Importer files
    go run cmd/main.go  --profile im-saasstage --bucket wireingest-filearea-1x1v8ekfnrc4z --filter ktd/wam --suffix xml

    go run cmd/main.go  --profile im-saasstage --bucket wireingest-filearea-1x1v8ekfnrc4z --filter ktd/wam --suffix jpg


## Demo view Open Content object stored in S3 bucket
    go run cmd/main.go  --profile gota --bucket gota-ocdata-imid-editorial/46ec818a-64fa-5a67-b81f-dea0509f617d/

