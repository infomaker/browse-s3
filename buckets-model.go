package browses3

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	tea "github.com/charmbracelet/bubbletea"
	"golang.org/x/term"
)

var (
	HelpTextBuckets = `# Browse buckets
- h or H                      | this information
- enter, space or right arrow | open selected folder / view file
- left arrow                  | go back a directory
- j or down arrow             | move cursor down
- k or up arrow               | move cursor up
- pgdown                      | next page 
- pgup                        | previous page
- ctrl+c or q                 | quit program
`
)

type Bucket struct {
	Name         string
	CreationDate time.Time
	Location     string
}

type BucketsModel struct {
	sess           *session.Session
	s3Svc          *s3.S3
	Buckets        []*Bucket
	cursor         int64
	windowSize     tea.WindowSizeMsg
	filter         string
	compiledFilter *regexp.Regexp
	start          int64
	limit          int64
}

func NewBucketsModel(sess *session.Session, filter string) (BucketsModel, error) {
	_, terminalHeight, err := term.GetSize(int(os.Stdout.Fd()))
	if err != nil {
		return BucketsModel{}, fmt.Errorf("failed to get terminal size when creating buckets model: %w", err)
	}

	pageSize := int64(terminalHeight - (headerHeight + footerHeight))

	compiledFilter, err := regexp.Compile(filter)
	if err != nil {
		return BucketsModel{}, fmt.Errorf("failed to compile filter %s:%w", filter, err)
	}

	return BucketsModel{
		sess:           sess,
		s3Svc:          s3.New(sess),
		filter:         filter,
		Buckets:        []*Bucket{},
		compiledFilter: compiledFilter,
		start:          0,
		limit:          pageSize,
	}, nil
}

func (b *BucketsModel) Load() error {
	// read buckets and Load first page
	input := s3.ListBucketsInput{}
	bucketsResp, err := b.s3Svc.ListBuckets(&input)
	if err != nil {
		return fmt.Errorf("failed to get buckets list: %w", err)
	}
	for _, bucket := range bucketsResp.Buckets {
		if b.PickRow(*bucket) {
			input := s3.GetBucketLocationInput{
				Bucket: bucket.Name,
			}
			var location = "[location miss]"
			locationResp, err := b.s3Svc.GetBucketLocation(&input)
			if err == nil && locationResp != nil && locationResp.LocationConstraint != nil {
				location = *locationResp.LocationConstraint
			}
			b.Buckets = append(b.Buckets, &Bucket{
				Name:         *bucket.Name,
				CreationDate: *bucket.CreationDate,
				Location:     location,
			})
		}
	}
	return nil
}

func (b *BucketsModel) PrevPage() {
	b.start -= b.limit
	b.cursor = b.start
	if b.start <= 0 {
		// first page
		b.start = 0
		b.cursor = 0
		return
	}
}

func (b *BucketsModel) NextPage() {
	if b.start+b.limit >= int64(len(b.Buckets)) {
		// last page
		return
	}
	// next page
	b.start += b.limit
	b.cursor = b.start
}

func (b *BucketsModel) Init() tea.Cmd {
	return nil
}

func (b *BucketsModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var (
		cmds []tea.Cmd
	)

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		// Info about the program
		case "h", "H":
			info := NewInfoModel(HelpTextBuckets)
			info.Parent = b
			return &info, func() tea.Msg {
				return b.windowSize
			}
		// These keys should exit the program.
		case "ctrl+c", "q":
			return b, tea.Quit
		// The "up" and "k" keys move the cursor up
		case "up", "k":

			if b.cursor > 0 && b.cursor > b.start {
				b.cursor--
			}
			if b.cursor > 0 && b.cursor <= b.start {
				b.start--
			}

		// The "down" and "j" keys move the cursor down
		case "down", "j":
			if b.cursor < int64(len(b.Buckets))-1 {
				b.cursor++
			}
			if b.cursor > b.start+b.limit {
				b.start++
			}

		// The "enter" key and the spacebar (a literal space) toggle
		// the selected state for the item that the cursor is pointing at.
		case "enter", " ", "right":
			model, _ := NewBucketModel(b.sess, b.Buckets[b.cursor].Name, "", "")
			model.parent = b
			_ = model.Load(true)
			return &model, func() tea.Msg {
				return b.windowSize
			}
		case "pgdown":
			b.NextPage()

		case "pgup":
			b.PrevPage()
		default:
			fmt.Println(msg)
		}
	case tea.WindowSizeMsg:
		// need to understand more
		b.windowSize = msg
		b.limit = int64(msg.Height - (headerHeight + footerHeight))
	}

	// Return the updated model to the Bubble Tea runtime for processing.
	// Note that we're not returning a command.
	return b, tea.Batch(cmds...)
}

func (b *BucketsModel) View() string {
	// The header

	// get the size of longest bucket name
	var longestBucketName = 50
	for _, choice := range b.Buckets {
		if len(choice.Name) > longestBucketName {
			longestBucketName = len(choice.Name)
		}
	}

	cols := fmt.Sprintf("%-1s %-"+strconv.Itoa(longestBucketName+2)+"s %-20s %-30s", "", "name", "region", "created")
	s := "Buckets \n" + cols + "\n"
	// Iterate over our options
	var row int64
	for i, choice := range b.Buckets {
		if int64(i) < b.start {
			continue
		}
		// Is the cursor pointing at this choice?
		cursor := " " // no cursor
		if b.cursor == int64(i) {
			cursor = ">" // cursor!
		}

		// Render the row
		s += fmt.Sprintf("%s %-"+strconv.Itoa(longestBucketName+2)+"s %-20s %-30s\n", cursor, choice.Name, choice.Location, choice.CreationDate.Format("2006-01-02 15:04:05 -0700 MST"))
		row++
		if row > b.limit {
			break
		}
	}

	// The footer
	s += "\nPress q to quit.\n"

	// Send the UI for rendering
	return s
}

func (b *BucketsModel) PickRow(bucket s3.Bucket) bool {
	if b.filter == "" {
		return true
	}
	// return strings.Contains(strings.ToLower(*bucket.Name), b.prefix)
	return b.compiledFilter.MatchString(*bucket.Name)
}
