package main

import (
	"fmt"
	"os"

	tea "github.com/charmbracelet/bubbletea"

	browses3 "bitbucket.org/infomaker/browse-s3"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/urfave/cli/v2"
)

func main() {
	if err := run(); err != nil {
		println(err.Error())
		os.Exit(1)
	}
}

func run() error {
	app := cli.App{
		Name:        "browse-s3",
		Description: "Browse AWS S3",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "profile",
				Usage:    "aws profile to use for access",
				Value:    "",
				Required: true,
			},
			&cli.StringFlag{
				Name:     "bucket",
				Usage:    "bucket to browse optional with path",
				Value:    "",
				Required: false,
			},
			&cli.StringFlag{
				Name:     "filter",
				Usage:    "regexp for bucket/file names",
				Value:    "",
				Required: false,
			},
			&cli.StringFlag{
				Name:     "day",
				Usage:    "date string (YYYY-MM-DD) to match buckets/files only if bucket is set",
				Value:    "",
				Required: false,
			},
		},
		Action: browseS3,
	}

	app.EnableBashCompletion = true

	return app.Run(os.Args)
}

func browseS3(context *cli.Context) error {
	bucket := context.String("bucket")
	profile := context.String("profile")
	filter := context.String("filter")
	day := context.String("day")

	sess, err := session.NewSession()
	if err != nil {
		return fmt.Errorf(
			"failed to create AWS SDK session: %w", err)
	}
	sess, err = browses3.AssumeProfile(sess, profile)
	if err != nil {
		return fmt.Errorf("failed to get AWS session: %w", err)
	}

	if bucket == "" {
		bm, err := browses3.NewBucketsModel(sess, filter)
		if err != nil {
			return fmt.Errorf("failed to create bucket model : %w", err)
		}
		err = bm.Load()
		if err != nil {
			return fmt.Errorf("failed to load buckets : %w", err)
		}
		if len(bm.Buckets) == 0 {
			return fmt.Errorf("nothing found")
		}
		p := tea.NewProgram(&bm)
		if err := p.Start(); err != nil {
			fmt.Printf("Alas, there's been an error: %v", err)
		}
	} else {
		bm, err := browses3.NewBucketModel(sess, bucket, filter, day)
		if err != nil {
			return fmt.Errorf("failed to create bucket model : %w", err)
		}

		err = bm.Load(true)
		if err != nil {
			return fmt.Errorf("failed to get next page : %w", err)
		}
		if len(bm.Files) == 0 {
			return fmt.Errorf("nothing found")
		}
		p := tea.NewProgram(&bm)
		if err := p.Start(); err != nil {
			fmt.Printf("Alas, there's been an error: %v", err)
		}
	}
	return err
}
