module bitbucket.org/infomaker/browse-s3

go 1.16

require (
	github.com/aws/aws-sdk-go v1.38.51
	github.com/charmbracelet/bubbles v0.7.9
	github.com/charmbracelet/bubbletea v0.13.4
	github.com/mattn/go-runewidth v0.0.12
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/urfave/cli/v2 v2.3.0
	golang.org/x/term v0.0.0-20210422114643-f5beecf764ed
	gopkg.in/ini.v1 v1.62.0
)
