package browses3

import (
	"bytes"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"

	"golang.org/x/term"

	"github.com/charmbracelet/bubbles/viewport"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	tea "github.com/charmbracelet/bubbletea"
)

type S3File struct {
	FileName string
	Modified *time.Time
	Size     int64
}

type BucketModel struct {
	sess           *session.Session
	s3Svc          *s3.S3
	filter         string
	compiledFilter *regexp.Regexp
	from           time.Time
	Files          []*S3File
	s3ListInput    s3.ListObjectsInput
	WindowSize     tea.WindowSizeMsg
	parent         tea.Model
	cursor         int64
	start          int64
	limit          int64

	Plugins map[string]func(bm *BucketModel) (tea.Model, tea.Cmd)
}

func trimFirstRune(s string) string {
	for i := range s {
		if i > 0 {
			// The value i is the index in s of the second
			// rune.  Slice to remove the first rune.
			return s[i:]
		}
	}
	// There are 0 or 1 runes in the string.
	return ""
}

func NewBucketModel(sess *session.Session, bucketFullPath string, filter string, day string) (BucketModel, error) {
	var err error

	// split into bucket and prefix
	var prefix string
	var bucket string
	parts := strings.Split(bucketFullPath, "/")
	bucket = parts[0]
	prefix = strings.ReplaceAll(bucketFullPath, bucket, "")

	if strings.HasPrefix(prefix, "/") {
		prefix = trimFirstRune(prefix)
	}

	_, terminalHeight, err := term.GetSize(int(os.Stdout.Fd()))
	if err != nil {
		return BucketModel{}, fmt.Errorf("failed to get terminal size: %w", err)
	}

	pageSize := int64(terminalHeight - (headerHeight + footerHeight))
	model := BucketModel{
		sess:  sess,
		s3Svc: s3.New(sess),
		Files: []*S3File{},
		s3ListInput: s3.ListObjectsInput{
			Bucket:              &bucket,
			Delimiter:           aws.String("/"),
			ExpectedBucketOwner: nil,
			Marker:              nil,
			MaxKeys:             nil,
			Prefix:              &prefix,
		},
		start:   0,
		limit:   pageSize,
		from:    time.Time{},
		Plugins: make(map[string]func(bm *BucketModel) (tea.Model, tea.Cmd)),
	}

	if day != "" {
		from, err := time.Parse(time.RFC3339, day+"T00:00:00Z")
		if err != nil {
			return model, fmt.Errorf("failed to parse from date: %w", err)
		}
		model.from = from
	}

	err = model.SetFilter(filter)
	if err != nil {
		return model, err
	}

	// do not know if this should be default
	_ = os.Chdir("/tmp")
	return model, nil
}

func (b *BucketModel) GetFile() string {
	return b.Files[b.cursor].FileName
}

func (b *BucketModel) SetFilter(filter string) error {
	compiledFilter, err := regexp.Compile(filter)
	if err != nil {
		return fmt.Errorf("failed to compile filter %s: %w", filter, err)
	}
	b.filter = filter
	b.compiledFilter = compiledFilter
	return nil
}

func (b *BucketModel) PickRow(file s3.Object) bool {
	if !b.from.IsZero() {
		if file.LastModified == nil {
			return false
		}
		y1, m1, d1 := file.LastModified.Date()
		y2, m2, d2 := b.from.Date()
		if !(y1 == y2 && m1 == m2 && d1 == d2) {
			return false
		}
	}

	if b.filter == "" {
		return true
	}
	return b.compiledFilter.MatchString(*file.Key)
}

func (b *BucketModel) Load(reload bool) error {
	if !reload {
		b.s3ListInput.Marker = &b.Files[len(b.Files)-1].FileName
	}

	if reload {
		b.Files = []*S3File{}
		b.cursor = 0
		b.s3ListInput.Marker = nil
		b.start = 0
	}

	for {
		objects, err := b.s3Svc.ListObjects(&b.s3ListInput)
		if err != nil {
			return fmt.Errorf("failed to list bucket: %s/%s :%w", *b.s3ListInput.Bucket, *b.s3ListInput.Prefix, err)
		}

		for _, row := range objects.CommonPrefixes {
			if !b.PickRow(s3.Object{
				Key: row.Prefix,
			}) {
				continue
			}
			b.Files = append(b.Files, &S3File{
				FileName: *row.Prefix,
			})
		}

		for _, row := range objects.Contents {
			if !b.PickRow(*row) {
				continue
			}
			b.Files = append(b.Files, &S3File{
				FileName: *row.Key,
				Modified: row.LastModified,
				Size:     *row.Size,
			})
		}

		if int64(len(b.Files)) >= b.limit || !*objects.IsTruncated {
			break
		}

		// new request with new marker
		b.s3ListInput.Marker = objects.NextMarker
	}
	return nil
}

func (b *BucketModel) PrevPage() {
	b.start -= b.limit
	b.cursor = b.start
	if b.start <= 0 {
		// first page
		b.start = 0
		b.cursor = 0
		return
	}
}

func (b *BucketModel) NextPage() {
	if b.start+b.limit >= int64(len(b.Files)) {
		_ = b.Load(false)
	}
	// next page
	b.start += b.limit
	b.cursor = b.start
}

func (b *BucketModel) Init() tea.Cmd {
	return nil
}

// fileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func (b *BucketModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var (
		// cmd  tea.Cmd
		cmds []tea.Cmd
	)
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "c", "C":
			return copyFileToLocal(b)
		case "l", "L":
			workingDir, _ := os.Getwd()
			model := initialModel("Working dir : ", workingDir)
			model.parent = b
			model.updateFunc = func(value string) {
				_ = os.Chdir(value)
			}
			return model, nil
		case "d", "D":
			if b.from.IsZero() {
				b.from = time.Now()
			}
			dayString := b.from.Format("2006-01-02")
			model := initialModel("Day for bucket : ", dayString)
			model.parent = b
			model.updateFunc = func(value string) {
				var err error
				var from time.Time

				from, err = time.Parse(time.RFC3339, value+"T00:00:00Z")
				if err != nil {
					b.from = time.Time{}
				}
				b.from = from
				_ = b.Load(true)
			}
			return model, nil
		case "f", "F":
			model := initialModel("Filter for bucket : ", b.filter)
			model.parent = b
			model.updateFunc = func(value string) {
				_ = b.SetFilter(value)
				_ = b.Load(true)
			}
			return model, nil
		case "p", "P":
			model := initialModel("Prefix for bucket : ", *b.s3ListInput.Prefix)
			model.parent = b
			model.updateFunc = func(value string) {
				b.s3ListInput.Prefix = &value
				_ = b.Load(true)
			}
			return model, nil
		case "esc":
			if b.parent != nil {
				return b.parent, nil
			}
		// These keys should exit the prograb.
		case "ctrl+c", "q":
			return b, tea.Quit
		// The "up" and "k" keys move the cursor up
		case "up", "k":
			if b.cursor > 0 && b.cursor > b.start {
				b.cursor--
			}
			if b.cursor > 0 && b.cursor <= b.start {
				b.start--
			}

		// The "down" and "j" keys move the cursor down
		case "down", "j":
			if b.cursor == int64(len(b.Files))-1 {
				// bottom reached
				_ = b.Load(false)
			}
			if b.cursor < int64(len(b.Files))-1 {
				b.cursor++
			}
			if b.cursor > b.start+b.limit {
				b.start++
			}

		case "left":
			return leaveDirectory(b)
		// The "enter" key and the spacebar (a literal space) toggle
		// the selected state for the item that the cursor is pointing at.
		case "enter", " ", "right":
			return viewOrOpenFile(b)
		case "pgdown":
			b.NextPage()
		case "pgup":
			b.PrevPage()
		default:
			if val, ok := b.Plugins[msg.String()]; ok {
				return val(b)
			}
			// print pressed key
			// fmt.Println(msg)
		}
	case tea.WindowSizeMsg:
		b.WindowSize = msg
		b.limit = int64(msg.Height - (headerHeight + footerHeight))
	}

	// Return the updated model to the Bubble Tea runtime for processing.
	// Note that we're not returning a command.
	return b, tea.Batch(cmds...)
}

func copyFileToLocal(b *BucketModel) (tea.Model, tea.Cmd) {
	workingDir, _ := os.Getwd()
	fileToCopy := workingDir + "/" + path.Base(b.Files[b.cursor].FileName)
	var prompt PromptModel
	if fileExists(fileToCopy) {
		prompt = PromptModel{
			parent:   b,
			question: "File " + fileToCopy + " already exists, overwrite",
			cursor:   1,
			options: []Option{
				{
					label: "yes",
					optionFunc: func() {
						input := s3.GetObjectInput{
							Bucket: b.s3ListInput.Bucket,
							Key:    &b.Files[b.cursor].FileName,
						}
						result, _ := b.s3Svc.GetObject(&input)
						buf := new(bytes.Buffer)
						_, _ = buf.ReadFrom(result.Body)

						out, _ := os.Create(workingDir + "/" + path.Base(b.Files[b.cursor].FileName))
						defer func() {
							_ = out.Close()
						}()
						_, _ = out.Write(buf.Bytes())
					},
				}, {
					label: "no",
					optionFunc: func() {
						// do nothing
					},
				},
			},
		}
	} else {
		prompt = PromptModel{
			parent:   b,
			question: "Copy file " + fileToCopy,
			cursor:   1,
			options: []Option{
				{
					label: "yes",
					optionFunc: func() {
						input := s3.GetObjectInput{
							Bucket: b.s3ListInput.Bucket,
							Key:    &b.Files[b.cursor].FileName,
						}
						result, _ := b.s3Svc.GetObject(&input)
						buf := new(bytes.Buffer)
						_, _ = buf.ReadFrom(result.Body)

						out, _ := os.Create(workingDir + "/" + path.Base(b.Files[b.cursor].FileName))
						defer func() {
							_ = out.Close()
						}()
						_, _ = out.Write(buf.Bytes())
					},
				},
				{
					label: "no",
					optionFunc: func() {
						// do nothing
					},
				},
			},
		}
	}
	return prompt, nil
}

func viewOrOpenFile(b *BucketModel) (tea.Model, tea.Cmd) {
	if len(b.Files) == 0 {
		// some info
		return b, nil
	}
	if b.Files[b.cursor].Size == 0 {
		newBucket, _ := NewBucketModel(b.sess, *b.s3ListInput.Bucket+"/"+b.Files[b.cursor].FileName, b.filter, "")
		newBucket.parent = b
		_ = newBucket.Load(true)
		return &newBucket, func() tea.Msg {
			return b.WindowSize
		}
	}

	// get the content of the selected file
	input := s3.GetObjectInput{
		Bucket: b.s3ListInput.Bucket,
		Key:    &b.Files[b.cursor].FileName,
	}
	result, _ := b.s3Svc.GetObject(&input)
	buf := new(bytes.Buffer)
	_, _ = buf.ReadFrom(result.Body)

	if isImage(buf.Bytes()) {
		out, _ := os.Create("/tmp/" + path.Base(b.Files[b.cursor].FileName))
		defer func() {
			_ = out.Close()
		}()
		_, err := out.Write(buf.Bytes())
		if err != nil {
			return nil, nil
		}

		cmd := exec.Command( //nolint:gosec
			"open",
			"file:///tmp/"+path.Base(b.Files[b.cursor].FileName),
		)

		if err := cmd.Start(); err != nil {
			fmt.Printf("can't open image: %s", err.Error())
		}
	} else {
		model := ViewModel{
			content:  buf.String(),
			ready:    false,
			viewport: viewport.Model{},
			fileName: b.Files[b.cursor].FileName,
			parent:   b,
		}
		return model, func() tea.Msg {
			return b.WindowSize
		}
	}
	return nil, nil
}

func leaveDirectory(b *BucketModel) (tea.Model, tea.Cmd) {
	if b.parent != nil {
		return b.parent, func() tea.Msg {
			return b.WindowSize
		}
	}

	// no prefix return Parent
	if *b.s3ListInput.Prefix == "" && b.parent != nil {
		return b.parent, func() tea.Msg {
			return b.WindowSize
		}
	}
	prefixString := *b.s3ListInput.Prefix
	if strings.HasSuffix(prefixString, "/") {
		prefixString = prefixString[:len(prefixString)-1]
	}
	// remove last part of the path
	var newPrefix string
	parts := strings.Split(prefixString, "/")
	if len(parts) > 0 {
		parts = parts[:len(parts)-1]
		newPrefix = strings.Join(parts, "/")
		if len(newPrefix) > 0 {
			newPrefix += "/"
		}
		b.s3ListInput.Prefix = &newPrefix
	} else {
		b.s3ListInput.Prefix = nil
	}
	err := b.Load(true)
	if err != nil {
		info := NewInfoModel("ERROR: \n" + err.Error())
		info.Parent = b
		return info, func() tea.Msg {
			return b.WindowSize
		}
	}
	return b, func() tea.Msg {
		return b.WindowSize
	}
}

func isImage(data []byte) bool {
	filetype := http.DetectContentType(data)
	switch filetype {
	case "image/jpeg", "image/jpg":
		return true
	case "image/gif":
		return true
	case "image/png":
		return true
	case "application/pdf": // not image, but application !
		return false
	default:
		return false
	}
}

func (b *BucketModel) View() string {
	var dateStr = "not set"
	if !b.from.IsZero() {
		dateStr = b.from.Format("2006-01-02")
	}
	workingDir, _ := os.Getwd()

	// get the size of longest bucket name
	var longestFileName = 50
	for _, choice := range b.Files {
		if len(choice.FileName) > longestFileName {
			longestFileName = len(choice.FileName)
		}
	}

	// The header
	cols := fmt.Sprintf("%-1s %-"+strconv.Itoa(longestFileName+2)+"s %-30s %7s", "", "key", "modified", "Size KB")
	s := "bucket:[" + *b.s3ListInput.Bucket + "/" + *b.s3ListInput.Prefix + "] filter:[" + b.filter + "] date :[" + dateStr + "]\ndir: [" + workingDir + "]\n" + cols + "\n"

	// Iterate over our options
	var row int64
	for i, choice := range b.Files {
		if int64(i) < b.start {
			continue
		}

		// Is the cursor pointing at this choice?
		cursor := " " // no cursor
		if b.cursor == int64(i) {
			cursor = ">" // cursor!
		}

		// Render the row
		var modified = ""
		if choice.Modified != nil {
			modified = choice.Modified.Format("2006-01-02 15:04:05 -0700 MST")
		}
		s += fmt.Sprintf("%s %5d %-"+strconv.Itoa(longestFileName+2)+"s %-30s %7d\n", cursor, i, choice.FileName, modified, (choice.Size / 1024))

		row++
		if row > b.limit {
			break
		}
	}

	// The footer
	s += "\nPress q to quit."

	// Send the UI for rendering
	return s
}
