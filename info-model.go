package browses3

import (
	tea "github.com/charmbracelet/bubbletea"
)

type InfoModel struct {
	Message string
	Parent  tea.Model
}

func NewInfoModel(message string) InfoModel {
	return InfoModel{
		Message: message,
		Parent:  nil,
	}
}

func (m InfoModel) Init() tea.Cmd {
	// Just return `nil`, which means "no I/O right now, please."
	return nil
}

func (m InfoModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	if msg, ok := msg.(tea.KeyMsg); ok {
		switch msg.String() {
		case "ctrl+c", "q":
			return m.Parent, tea.Quit
		default:
			// exit for any key
			return m.Parent, nil
		}
	}

	// Return the updated model to the Bubble Tea runtime for processing.
	// Note that we're not returning a command.
	return m, nil
}

func (m InfoModel) View() string {
	// The header
	s := "INFO\n\n"

	s += m.Message

	// The footer
	s += "\nPress any key ...\n"

	// Send the UI for rendering
	return s
}
