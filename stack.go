package browses3

type Stack []*string

// IsEmpty check if stack is empty
func (s *Stack) IsEmpty() bool {
	return len(*s) == 0
}

// Push a new value onto the stack
func (s *Stack) Push(str *string) {
	*s = append(*s, str) // Simply append the new value to the end of the stack
}

func (s *Stack) Peek() *string {
	if s.IsEmpty() {
		return nil
	}
	return (*s)[len(*s)-1] // Get the index of the top most element.
}

// Pop Remove and return top element of stack. Return nil is stack is empty
func (s *Stack) Pop() *string {
	if s.IsEmpty() {
		return nil
	}
	index := len(*s) - 1   // Get the index of the top most element.
	element := (*s)[index] // Index into the slice and obtain the element.
	*s = (*s)[:index]      // Remove it from the stack by slicing it off.
	return element
}
