package browses3

import (
	"fmt"

	tea "github.com/charmbracelet/bubbletea"
)

type Option struct {
	label      string
	optionFunc func()
}

type PromptModel struct {
	options  []Option
	cursor   int
	parent   tea.Model
	question string
}

func (m PromptModel) Init() tea.Cmd {
	// Just return `nil`, which means "no I/O right now, please."
	return nil
}

func (m PromptModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	if msg, ok := msg.(tea.KeyMsg); ok {
		switch msg.String() {
		// These keys should exit the program.
		case "ctrl+c", "q":
			return m, tea.Quit

		// The "up" and "k" keys move the cursor up
		case "up", "k":
			if m.cursor > 0 {
				m.cursor--
			}

		// The "down" and "j" keys move the cursor down
		case "down", "j":
			if m.cursor < len(m.options)-1 {
				m.cursor++
			}

		// The "enter" key and the spacebar (a literal space) toggle
		// the selected state for the item that the cursor is pointing at.
		case "enter", " ":
			m.options[m.cursor].optionFunc()
			return m.parent, nil
		}
	}

	// Return the updated model to the Bubble Tea runtime for processing.
	// Note that we're not returning a command.
	return m, nil
}

func (m PromptModel) View() string {
	// The header
	s := m.question + "?\n\n"

	// Iterate over our options
	for i, option := range m.options {
		// Is the cursor pointing at this option?
		cursor := " " // no cursor
		if m.cursor == i {
			cursor = ">" // cursor!
		}

		// Render the row
		s += fmt.Sprintf("%s %s\n", cursor, option.label)
	}

	// The footer
	s += "\nPress enter to select.\n"

	// Send the UI for rendering
	return s
}
