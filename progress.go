package browses3

import (
	"fmt"

	"github.com/charmbracelet/bubbles/spinner"
	tea "github.com/charmbracelet/bubbletea"
)

type ProgressModel struct {
	message  string
	spinner  spinner.Model
	quitting bool
	err      error
	parent   tea.Model
}

func NewProgressModel(message string, parent tea.Model) (ProgressModel, error) {
	return ProgressModel{
		message:  message,
		spinner:  spinner.Model{},
		quitting: false,
		err:      nil,
		parent:   parent,
	}, nil
}

func (m ProgressModel) Init() tea.Cmd {
	return spinner.Tick
}

func (m ProgressModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "q", "esc", "ctrl+c":
			m.quitting = true
			return m, tea.Quit
		default:
			return m, nil
		}

	case errMsg:
		m.err = msg
		return m, nil

	default:
		var cmd tea.Cmd
		m.spinner, cmd = m.spinner.Update(msg)
		return m, cmd
	}
}

func (m ProgressModel) View() string {
	if m.err != nil {
		return m.err.Error()
	}
	str := fmt.Sprintf("\n\n   %s %s\n\n", m.spinner.View(), m.message)
	if m.quitting {
		return str + "\n"
	}
	return str
}
